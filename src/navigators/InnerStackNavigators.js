import { createStackNavigator } from 'react-navigation-stack';
import {
  FeedScreen,
  DetailPostScreen,
  CreatePostScreen,
  DiscoverScreen,
  ProfileScreen,
  ChatScreen,
  WatchScreen,
  MarketplaceScreen,
  DetailMarketplaceScreen,
  NewOfferScreen,
  PaymentScreen,
  BecomeSellerScreen,
  MenuScreen,
  UserPagesScreen,
  NewPageCategoryScreen,
  NewPageNameScreen,
  NewGroupScreen,
  GuissScreen,
  AddGroup,
  GroupDetail,
} from '../screens';
import { IMCreateGroupScreen } from '../Core/chat';
import {
  IMFriendsScreen,
  IMAllFriendsScreen,
} from '../Core/socialgraph/friendships';
import {
  IMEditProfileScreen,
  IMUserSettingsScreen,
  IMContactUsScreen,
  IMProfileSettingsScreen,
} from '../Core/profile';
import { IMNotificationScreen } from '../Core/notifications';
import AppStyles from '../AppStyles';
import SocialNetworkConfig from '../SocialNetworkConfig';
import { IMLocalized } from '../Core/localization/IMLocalization';
import { Platform } from 'react-native';
import CartScreen from '../screens/CartScreen/CartScreen';

const InnerFeedNavigator = createStackNavigator(
  {
    Feed: { screen: FeedScreen },
    FeedDetailPost: { screen: DetailPostScreen },
    CreatePost: {
      screen: CreatePostScreen,
    },
    FeedProfile: { screen: ProfileScreen },
    FeedNotification: { screen: IMNotificationScreen },
    FeedProfileSettings: { screen: IMProfileSettingsScreen },
    FeedEditProfile: { screen: IMEditProfileScreen },
    FeedAppSettings: { screen: IMUserSettingsScreen },
    FeedContactUs: { screen: IMContactUsScreen },
    FeedAllFriends: { screen: IMAllFriendsScreen },
    FeedDiscover: { screen: DiscoverScreen },
    CartScreen: { screen: CartScreen },
    PaymentScreen: { screen: PaymentScreen },

    ChatMain: { screen: ChatScreen },
    CreateGroup: { screen: IMCreateGroupScreen },
  },
  {
    initialRouteName: 'Feed',
    headerMode: 'float',
    headerLayoutPreset: 'center',
  },
);

const InnerMarketplaceNavigator = createStackNavigator(
  {
    Marketplace: { screen: MarketplaceScreen },
    DetailMarketplace: { screen: DetailMarketplaceScreen },
    Payment: { screen: PaymentScreen },
    BecomeSeller: { screen: BecomeSellerScreen },
    NewOffer: { screen: NewOfferScreen },
  },
  {
    initialRouteName: 'Marketplace',
    headerMode: 'float',
    headerLayoutPreset: 'center',
  },
);

const InnerFriendsNavigator = createStackNavigator(
  {
    Friends: { screen: IMFriendsScreen },
    FriendsProfile: { screen: ProfileScreen },
    FriendsAllFriends: { screen: IMAllFriendsScreen },
  },
  {
    initialRouteName: 'Friends',
    initialRouteParams: {
      appStyles: AppStyles,
      appConfig: SocialNetworkConfig,
      followEnabled: false,
      friendsScreenTitle: IMLocalized('Friends'),
      showDrawerMenuButton: Platform.OS === 'android',
    },
    headerMode: 'float',
    headerLayoutPreset: 'center',
  },
);
const InnerDiscoverNavigator = createStackNavigator(
  {
    DiscoverMain: { screen: DiscoverScreen },
    DiscoverVideo: { screen: WatchScreen },
    DiscoverDetailPost: { screen: DetailPostScreen },
    DiscoverProfile: { screen: ProfileScreen },
    DiscoverNotification: { screen: IMNotificationScreen },
    DiscoverProfileSettings: { screen: IMProfileSettingsScreen },
    DiscoverEditProfile: { screen: IMEditProfileScreen },
    DiscoverAppSettings: { screen: IMUserSettingsScreen },
    DiscoverContactUs: { screen: IMContactUsScreen },
    DiscoverAllFriends: { screen: IMAllFriendsScreen },
  },
  {
    initialRouteName: 'DiscoverVideo',
    headerMode: 'float',
    headerLayoutPreset: 'center',
  },
);

const InnerProfileNavigator = createStackNavigator(
  {
    Profile: { screen: ProfileScreen },
    ProfileNotification: { screen: IMNotificationScreen },
    ProfileProfileSettings: { screen: IMProfileSettingsScreen },
    ProfileEditProfile: { screen: IMEditProfileScreen },
    ProfileAppSettings: { screen: IMUserSettingsScreen },
    ProfileContactUs: { screen: IMContactUsScreen },
    ProfileAllFriends: { screen: IMAllFriendsScreen },
    ProfilePostDetails: { screen: DetailPostScreen },
    ProfileDetailPostProfile: { screen: ProfileScreen },
  },
  {
    initialRouteName: 'ProfileNotification',
    headerMode: 'float',
    headerLayoutPreset: 'center',
  },
);

const InnerMenuNavigator = createStackNavigator(
  {
    Menu: { screen: MenuScreen },
    UserPages: { screen: UserPagesScreen },
    NewPageCategory: { screen: NewPageCategoryScreen },
    NewPageName: { screen: NewPageNameScreen },
    NewGroupScreen: { screen: NewGroupScreen },
    GuissScreen: { screen: GuissScreen },
    AddGroup: { screen: AddGroup },
    PageDetail: { screen: ProfileScreen },
    PageCreatePost: { screen: CreatePostScreen },
    GroupDetail: {
      screen: GroupDetail,
    },
  },
  {
    initialRouteName: 'Menu',
    headerMode: 'float',
    headerLayoutPreset: 'center',
  },
);

export {
  InnerFeedNavigator,
  InnerMarketplaceNavigator,
  InnerFriendsNavigator,
  InnerDiscoverNavigator,
  InnerProfileNavigator,
  InnerMenuNavigator,
};
