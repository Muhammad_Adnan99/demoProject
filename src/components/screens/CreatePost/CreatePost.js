import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Platform, ImageBackground,
  KeyboardAvoidingView, Keyboard,
  BackHandler, Animated, Dimensions
} from 'react-native';
import { withNavigation } from 'react-navigation';

import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-actionsheet';
import { Video } from 'expo-av';
import { useColorScheme } from 'react-native-appearance';
import FastImage from 'react-native-fast-image';
import { createImageProgress } from 'react-native-image-progress';
import { TNStoryItem, TNTouchableIcon } from '../../../Core/truly-native';
import IMLocationSelectorModal from '../../../Core/location/IMLocationSelectorModal/IMLocationSelectorModal';
import IMCameraModal from '../../../Core/camera/IMCameraModal';
import { IMLocalized } from '../../../Core/localization/IMLocalization';
import { extractSourceFromFile } from '../../../Core/helpers/retrieveSource';
import dynamicStyles from './styles';
import AppStyles from '../../../AppStyles';
import * as Progress from 'react-native-progress';
import { PanGestureHandler, State, TouchableWithoutFeedback } from 'react-native-gesture-handler'

const screenHeight = Math.round(Dimensions.get('window').height);
const screenWidth = Math.round(Dimensions.get('window').width);
// https://stackoverflow.com/questions/17901692/set-up-adb-on-mac-os-x
// import Marker, { Position, ImageFormat } from 'react-native-image-marker'

const Image = createImageProgress(FastImage);

const bgColors = [
  {
    "id": 0,
    "group": "Popular",
    "isPureColor": true,
    "color": "#fff",
    "textColor": "#000"
  },
  {
    "id": 1,
    "group": "Popular",
    "isPureColor": true,
    "color": "#000",
    "textColor": "#fff"
  },
  {
    "id": 2,
    "group": "New",
    "isPureColor": true,
    "color": "#020",
    "textColor": "#fff"
  },
  {
    "id": 3,
    "group": "New",
    "isPureColor": false,
    "bgImage_url": "https://voyagerloin.com/wp-content/uploads/2014/01/5370010626_2f20427864_b.jpg",
    "textColor": "#fff"
  },
  {
    "id": 4,
    "group": "New",
    "isPureColor": false,
    "bgImage_url": "https://voyagerloin.com/wp-content/uploads/2014/01/2fbf809423b14e3bfb78f27bd0effde6.jpg",
    "textColor": "#fff"
  },
  {
    "id": 5,
    "group": "Popular",
    "isPureColor": false,
    "bgImage_url": "https://voyagerloin.com/wp-content/uploads/2014/01/kubu-island-6.jpg",
    "textColor": "#fff"
  },
  {
    "id": 6,
    "group": "Popular",
    "isPureColor": false,
    "bgImage_url": "https://image.shutterstock.com/image-vector/trendy-gradient-liquid-background-lines-260nw-1375011146.jpg",
    "textColor": "#fff"
  },
  {
    "id": 7,
    "group": "New",
    "isPureColor": false,
    "bgImage_url": "https://voyagerloin.com/wp-content/uploads/2014/01/drakensberg-escarpment-2013-8.jpg",
    "textColor": "#fff"
  },
  {
    "id": 8,
    "group": "Other",
    "isPureColor": false,
    "bgImage_url": "https://image.shutterstock.com/image-photo/blue-white-background-new-year-260nw-1231287469.jpg",
    "textColor": "#000"
  },
  {
    "id": 9,
    "group": "Other",
    "isPureColor": false,
    "bgImage_url": "https://media.istockphoto.com/vectors/abstract-green-and-blue-low-poly-triangle-bg-vector-id952458018?k=6&m=952458018&s=170667a&w=0&h=PLP72y8WaPW6PVKM1Nle36uJhGp_qamHjAa3Zx8-LGQ=",
    "textColor": "#000"
  }
]
const _editorWrapperHeight = new Animated.Value(100)

function CreatePost(props) {
  const {
    onPostDidChange,
    onSetMedia,
    onLocationDidChange,
    user,
    inputRef,
    blurInput,
    loader
  } = props;
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(colorScheme);

  const [address, setAddress] = useState('');
  const [locationSelectorVisible, setLocationSelectorVisible] = useState(false);
  const [media, setMedia] = useState([]);
  const [mediaSources, setMediaSources] = useState([]);
  const [value, setValue] = useState('');
  const [heiht, setHeiht] = useState(0);
  const [isCameralContainer, setIsCameralContainer] = useState(false);
  const [isCameraOpen, setIsCameraOpen] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(null);
  const photoUploadDialogRef = useRef();
  const removePhotoDialogRef = useRef();
  const [selectedBgColorId, setSelectedBgColorId] = useState(0);
  const [selectedBgColor, setSelectedBgColor] = useState(bgColors[0]);
  const [isTextExceeded, setIsTextExceeded] = useState(false);
  const [backgroundPickCol, setBackgroundPickCol] = useState('#fff');


  let _isShowBgColors = true
  let _bgColorListWidth = new Animated.Value(screenWidth - 60)
  let _toggleZindexValue = new Animated.Value(2)
  let _degTransformToggle = new Animated.Value(0)
  let _scaleTransformToggle = new Animated.Value(0)
  let _isKeyBoardVisibled = false
  let _distanceTopOption = new Animated.Value(0)
  let _prevTranslatetionY = 0


  const androidAddPhotoOptions = [
    IMLocalized('Importer depuis la bibliothèque'),
    IMLocalized('Prendre une photo'),
    IMLocalized('Enregistrer une vidéo'),
    IMLocalized('Annuler'),
  ];

  const iosAddPhotoOptions = [
    IMLocalized('Importer depuis la bibliothèque'),
    IMLocalized('Ouvrir la caméra'),
    IMLocalized('Annuler'),
  ];

  const addPhotoCancelButtonIndex = {
    ios: 2,
    android: 3,
  };

  const addPhotoOptions =
    Platform.OS === 'android' ? androidAddPhotoOptions : iosAddPhotoOptions;

  function handleBackButtonClick() {
    props.navigation.goBack();
    return true;
  }

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    let keyboardDidShowListener = Keyboard.addListener('keyboardDidShow',
      _keyboardDidShow,
    );
    let keyboardWillShowListener = Keyboard.addListener('keyboardWillShow',
      _keyboardWillShow,
    );
    let keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',
      _keyboardDidHide,
    );
    if (bgColors.length === 0) return;

    preloadBgImages(bgColors)

    return () => {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        handleBackButtonClick,
      );
    };
  }, []);

  const _keyboardWillShow = () => {
    _distanceTopOption.setValue(0)
    _prevTranslatetionY = 0
  }
  const _keyboardDidShow = () => {
    _isKeyBoardVisibled = true
    if (!_isShowBgColors) {
      Animated.timing(_scaleTransformToggle, {
        toValue: 0,
        duration: 100
      }).start(() => {
        _toggleZindexValue.setValue(2)
        Animated.timing(_degTransformToggle, {
          toValue: 0,
          duration: 200
        }).start(() => { })
      })
      Animated.spring(_bgColorListWidth, {
        toValue: screenWidth - 60,
        duration: 300
      }).start(() => {
        _isShowBgColors = true
      })

    }
  }



  const onContentSizeChangeHandler = ({ nativeEvent }) => {
    const { height } = nativeEvent.contentSize
    Animated.timing(_editorWrapperHeight, {
      toValue: height + 20,
      duration: 0
    }).start()
  }
  const onGestureEventHandler = ({ nativeEvent }) => {
    if (!_isKeyBoardVisibled) {
      const { translationY } = nativeEvent
      if (_prevTranslatetionY - translationY > 610) return;
      _distanceTopOption.setValue(_prevTranslatetionY - translationY)
    }
  }
  const onHandlerStateChangeHandler = ({ nativeEvent }) => {
    if (_isKeyBoardVisibled) return;
    if (nativeEvent.state === State.END) {
      let { translationY } = nativeEvent
      translationY = _prevTranslatetionY - translationY
      if (Math.abs(translationY) < 150) {
        Animated.spring(_distanceTopOption, {
          toValue: 0,
          duration: 200
        }).start(() => _prevTranslatetionY = 0)

      } else if (Math.abs(translationY) > 150 && Math.abs(translationY) < 350) {
        Animated.spring(_distanceTopOption, {
          toValue: 247.5,
          duration: 200
        }).start(() => _prevTranslatetionY = 247.5)

      } else {
        Animated.spring(_distanceTopOption, {
          toValue: 600,
          duration: 200
        }).start(() => _prevTranslatetionY = 600)

      }
    }
  }
  const onSelectBgColorHandler = async (bgColorId) => {
    // this.setState({
    //   ...this.state,
    //   selectedBgColorId: bgColorId
    // })
    const selectedBgColor = bgColors.filter((bgColor) => bgColor.id === bgColorId)[0]

    setSelectedBgColor(selectedBgColor)

    setSelectedBgColorId(bgColorId)


  }
  const onTogglebBgColorListHandler = () => {
    if (!_isShowBgColors) {
      Animated.timing(_scaleTransformToggle, {
        toValue: 0,
        duration: 100
      }).start(() => {
        _toggleZindexValue.setValue(2)
        Animated.timing(_degTransformToggle, {
          toValue: 0,
          duration: 200
        }).start(() => { })
      })
      Animated.spring(_bgColorListWidth, {
        toValue: screenWidth - 60,
        duration: 300
      }).start(() => {
        _isShowBgColors = true
      })
    } else {
      Animated.timing(_degTransformToggle, {
        toValue: -90,
        duration: 100
      }).start(() => {
        _toggleZindexValue.setValue(0)
        Animated.timing(_scaleTransformToggle, {
          toValue: 1,
          duration: 200
        }).start(() => { })
      })
      Animated.timing(_bgColorListWidth, {
        toValue: 0,
        duration: 300
      }).start(() => {
        _isShowBgColors = false
      })
    }

  }
  const onPressShowOptions = () => {
    Keyboard.dismiss()
    if (_prevTranslatetionY == 0) {
      Animated.spring(_distanceTopOption, {
        toValue: 247.5,
        duration: 200
      }).start(() => _prevTranslatetionY = 247.5)
    } else if (_prevTranslatetionY === 247.5) {
      Animated.spring(_distanceTopOption, {
        toValue: 600,
        duration: 200
      }).start(() => _prevTranslatetionY = 600)
    } else {
      Animated.spring(_distanceTopOption, {
        toValue: 247.5,
        duration: 200
      }).start(() => _prevTranslatetionY = 247.5)
    }
  }

  const _keyboardDidHide = () => {
    _isKeyBoardVisibled = false
  }

  const onLocationSelectorPress = () => {
    setLocationSelectorVisible(!locationSelectorVisible);
  };

  const onLocationSelectorDone = (address) => {
    setLocationSelectorVisible(!locationSelectorVisible);
    setAddress(address);
  };

  const onChangeLocation = (address) => {
    setAddress(address);
    onLocationDidChange(address);
  };

  const onChangeText = (value) => {
    const Post = {
      postText: value,
      commentCount: 0,
      reactionsCount: 0,
      reactions: {
        surprised: 0,
        angry: 0,
        sad: 0,
        laugh: 0,
        like: 0,
        cry: 0,
        love: 0,
      },
    };

    setValue(value);
    onPostDidChange(Post);

    if (value.length > 140) {
      setIsTextExceeded(true)
    } else {
      setIsTextExceeded(false)

    }
  };

  const onCameraIconPress = () => {
    if (Platform.OS === 'ios') {
      setIsCameraOpen(true);
    } else {
      photoUploadDialogRef.current.show();
    }
  };

  const preloadBgImages = (bgImages) => {
    let preFetchTasks = [];
    for (let bgImage of bgImages) {
      if (!bgImage.isPureColor) {
        preFetchTasks.push(Image.prefetch(bgImage.bgImage_url));
      }
    }
    Promise.all(preFetchTasks).then((results) => {
      let downloadedAll = true;
      results.forEach((result) => {
        if (!result) {
          downloadedAll = false;
        }
      })
    })
  }

  const onPhotoUploadDialogDoneIOS = (index) => {
    if (index == 1) {
      onLaunchCamera();
    }

    if (index == 0) {
      onOpenPhotos();
    }
  };

  const onPhotoUploadDialogDoneAndroid = (index) => {
    if (index == 2) {
      onLaunchVideoCamera();
    }

    if (index == 1) {
      onLaunchCamera();
    }

    if (index == 0) {
      onOpenPhotos();
    }
  };

  const onPhotoUploadDialogDone = (index) => {
    const onPhotoUploadDialogDoneSetter = {
      ios: () => onPhotoUploadDialogDoneIOS(index),
      android: () => onPhotoUploadDialogDoneAndroid(index),
    };

    onPhotoUploadDialogDoneSetter[Platform.OS]();
  };

  const onLaunchCamera = () => {
    ImagePicker.openCamera({
      cropping: false,
    }).then((image) => {
      const { source, mime, filename, uploadUri } = extractSourceFromFile(
        image,
      );

      setMedia([...media, { source, mime }]);
      setMediaSources([...mediaSources, { filename, uploadUri, mime }]);
      onSetMedia([...mediaSources, { filename, uploadUri, mime }]);
    });
  };

  const onLaunchVideoCamera = () => {
    ImagePicker.openCamera({
      cropping: false,
      mediaType: 'video',
    }).then((image) => {
      const { source, mime, filename, uploadUri } = extractSourceFromFile(
        image,
      );

      setMedia([...media, { source, mime }]);
      setMediaSources([...mediaSources, { filename, uploadUri, mime }]);
      onSetMedia([...mediaSources, { filename, uploadUri, mime }]);
    });
  };

  const onOpenPhotos = () => {
    ImagePicker.openPicker({
      cropping: false,
      multiple: true,
    }).then((image) => {
      const newPhotos = [];
      const sources = image.map((image) => {
        const { source, mime, filename, uploadUri } = extractSourceFromFile(
          image,
        );

        newPhotos.push({ source, mime });

        return { filename, uploadUri, mime };
      });
      setMedia([...media, ...newPhotos]);
      setMediaSources([...mediaSources, ...sources]);
      onSetMedia([...mediaSources, ...sources]);
    });
  };

  const onRemovePhotoDialogDone = (index) => {
    if (index === 0) {
      removePhoto();
    } else {
      setSelectedIndex(null);
    }
  };

  const onMediaPress = async (index) => {
    await setSelectedIndex(index);
    removePhotoDialogRef.current.show();
  };

  const onCameraClose = () => {

    setIsCameraOpen(false);
  };

  const onImagePost = (item) => {
    const { source, mime, filename, uploadUri } = extractSourceFromFile({
      uri: item.uri,
      mime: item.mime,
    });
    setMedia([...media, { source, mime }]);
    setMediaSources([...mediaSources, { filename, uploadUri, mime }]);
    onSetMedia([...mediaSources, { filename, uploadUri, mime }]);
  };

  const removePhoto = async () => {
    const slicedMedia = [...media];
    const slicedMediaSources = [...mediaSources];
    await slicedMedia.splice(selectedIndex, 1);
    await slicedMediaSources.splice(selectedIndex, 1);
    setMedia([...slicedMedia]);
    setMediaSources([...slicedMediaSources]);
    onSetMedia([...slicedMediaSources]);
  };

  const onTextFocus = () => {
    // setIsCameralContainer(false);
  };

  const onToggleImagesContainer = () => {
    blurInput();
    toggleImagesContainer();
  };

  const toggleImagesContainer = () => {
    setIsCameralContainer(!isCameralContainer);
  };

  const onStoryItemPress = (item) => {
    console.log('');
  };


  return (

    <KeyboardAvoidingView
      behavior={Platform.OS == "ios" ? "padding" : "height"}
      // enabled={false} 
      style={styles.container}>
      <View style={styles.topContainer}>
        <View style={styles.headerContainer}>
          <TNStoryItem
            onPress={onStoryItemPress}
            item={user}
            imageContainerStyle={styles.userIconContainer}
            imageStyle={styles.userIcon}
            appStyles={AppStyles}
          />
          <View style={styles.titleContainer}>
            <Text style={styles.title}>{user.pageName || user.firstName}</Text>
            <Text style={styles.subtitle}>{address}</Text>
          </View>
        </View>
        {isTextExceeded && (
          <View style={styles.postInputContainer}>
            <TextInput
              placeholder={`Exprime ta pensée`}
              placeholderTextColor="grey"
              ref={inputRef}
              style={[styles.postInput, { backgroundColor: '#3a3a3a', height: Math.max(35, heiht) }]}
              onChangeText={onChangeText}
              value={value}
              multiline={true}
              editable={true}
              onFocus={onTextFocus}
              onContentSizeChange={(event) => {
                setHeiht(event.nativeEvent.contentSize.height)
              }}
            />
          </View>
        )}

        {selectedBgColor && !isTextExceeded &&
          <ImageBackground source={!selectedBgColor.isPureColor ? { uri: selectedBgColor.bgImage_url } : {}} style={{ ...styles.editorWrapper, backgroundColor: selectedBgColor.isPureColor ? selectedBgColor.color : '' }}>
            <Animated.View style={{
              height: _editorWrapperHeight,
              alignSelf: 'stretch',
              width: '100%',
              justifyContent: 'center',
            }}>
              <TextInput
                onChangeText={onChangeText}
                ref={inputRef}
                value={value}
                onContentSizeChange={onContentSizeChangeHandler}
                placeholderTextColor={selectedBgColor.textColor}
                placeholder={`Exprime ta pensée`}
                multiline style={{
                  ...styles.editor, fontSize: 26,
                  textAlign: 'center', color: selectedBgColor.textColor, fontWeight: 'bold'
                }}>
              </TextInput>
            </Animated.View>
          </ImageBackground>
        }





      </View>
      <View style={[styles.bottomContainer]}>
        {!isTextExceeded && (
          <Animated.View style={styles.toolOptionsWrapper}>
            <View style={styles.bgColorsWrapper}>
              <TouchableWithoutFeedback style={styles.btnBgColor} onPress={onTogglebBgColorListHandler}>
                <Animated.Image style={{ ...styles.bgImage, ...styles.toggleBgColors, zIndex: _toggleZindexValue }} source={require('../../../../assets/icons/left-arrow.png')} />
                <Animated.Image style={{ ...styles.bgImage, ...styles.toggleBgColors, zIndex: 1, transform: [{ scale: _scaleTransformToggle }] }} source={require('../../../../assets/icons/letter-a.png')} />
              </TouchableWithoutFeedback>
              <Animated.View style={{ flexDirection: 'row', width: _bgColorListWidth }}>
                <ScrollView horizontal={true} style={{ ...styles.bgColorsScrollView }} showsHorizontalScrollIndicator={false}>
                  {bgColors.map((bgColor, index) => (
                    <View key={index}>
                      {bgColor.isPureColor &&
                        <TouchableWithoutFeedback style={styles.bgColor} onPress={() => onSelectBgColorHandler(bgColor.id)}>
                          <View style={{ backgroundColor: bgColor.color, ...styles.bgImage, borderColor: selectedBgColorId === bgColor.id ? '#318bfb' : '#333', borderWidth: selectedBgColorId === bgColor.id ? 3 : 1 }}></View>
                        </TouchableWithoutFeedback>
                      }
                      {!bgColor.isPureColor &&
                        <TouchableWithoutFeedback style={styles.bgColor} onPress={() => onSelectBgColorHandler(bgColor.id)}>
                          <Image style={{ ...styles.bgImage, borderColor: selectedBgColorId === bgColor.id ? '#318bfb' : '#333', borderWidth: selectedBgColorId === bgColor.id ? 3 : 1 }} source={{ uri: bgColor.bgImage_url }}></Image>
                        </TouchableWithoutFeedback>
                      }

                    </View>
                  ))}
                </ScrollView>
                <TouchableWithoutFeedback style={styles.btnBgColor} onPress={() => console.log("okeee")}>
                  <Image style={{ ...styles.bgImage, ...styles.moreBgColors }} source={require('../../../../assets/icons/more.png')}></Image>
                </TouchableWithoutFeedback>
              </Animated.View>
            </View>

          </Animated.View>

        )}
        <View style={styles.postImageAndLocationContainer}>

          <ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={[
              styles.imagesContainer,
              isCameralContainer ? { display: 'flex' } : { display: 'none' },
            ]}>
            {media.map((singleMedia, index) => {
              const { source, mime } = singleMedia;

              if (mime.startsWith('image')) {
                return (
                  <TouchableOpacity
                    activeOpacity={0.9}
                    onPress={() => onMediaPress(index)}
                    style={styles.imageItemcontainer}>
                    <Image style={styles.imageItem} source={{ uri: source }} />
                  </TouchableOpacity>
                );
              } else {
                return (
                  <TouchableOpacity
                    activeOpacity={0.9}
                    onPress={() => onMediaPress(index)}
                    style={styles.imageItemcontainer}>
                    <Video
                      source={{
                        uri: source,
                      }}
                      resizeMode={'cover'}
                      shouldPlay={false}
                      isMuted={true}
                      style={styles.imageItem}
                    />
                  </TouchableOpacity>
                );
              }
            })}
            <TouchableOpacity
              onPress={onCameraIconPress}
              style={[styles.imageItemcontainer, styles.imageBackground]}>
              <Image
                style={styles.addImageIcon}
                source={AppStyles.iconSet.cameraFilled}
              />
            </TouchableOpacity>
          </ScrollView>
          <View style={[styles.addTitleAndlocationIconContainer]}>
            <View style={styles.addTitleContainer}>
              {!loader ?
                (
                  <Text style={styles.addTitle}>
                    {!isCameralContainer
                      ? IMLocalized('Ajoute du contenu à ta publication')
                      : IMLocalized('Ajoute une photo à ta publication')}
                  </Text>
                ) : (
                  <Progress.Bar indeterminate={true} progress={0.3} width={200} />
                )}

            </View>
            <View style={styles.iconsContainer}>
              <TNTouchableIcon
                onPress={onToggleImagesContainer}
                containerStyle={styles.iconContainer}
                imageStyle={[
                  styles.icon,
                  isCameralContainer
                    ? styles.cameraFocusTintColor
                    : styles.cameraUnfocusTintColor,
                ]}
                iconSource={AppStyles.iconSet.cameraFilled}
                appStyles={AppStyles}
              />
              <TNTouchableIcon
                containerStyle={styles.iconContainer}
                imageStyle={[styles.icon, styles.pinpointTintColor]}
                iconSource={AppStyles.iconSet.pinpoint}
                onPress={onLocationSelectorPress}
                appStyles={AppStyles}
              />
            </View>
          </View>
        </View>
      </View>
      <View style={styles.blankBottom} />

      <IMLocationSelectorModal
        isVisible={locationSelectorVisible}
        onCancel={onLocationSelectorPress}
        onDone={onLocationSelectorDone}
        onChangeLocation={onChangeLocation}
        appStyles={AppStyles}
      />
      <ActionSheet
        ref={photoUploadDialogRef}
        title={IMLocalized('Ajouter une photo')}
        options={addPhotoOptions}
        cancelButtonIndex={addPhotoCancelButtonIndex[Platform.OS]}
        onPress={onPhotoUploadDialogDone}
      />
      <ActionSheet
        ref={removePhotoDialogRef}
        title={IMLocalized('Supprimer la photo')}
        options={[IMLocalized('Supprimer'), IMLocalized('Annuler')]}
        destructiveButtonIndex={0}
        cancelButtonIndex={1}
        onPress={onRemovePhotoDialogDone}
      />
      <IMCameraModal
        isCameraOpen={isCameraOpen}
        onImagePost={onImagePost}
        onCameraClose={onCameraClose}
      />
    </KeyboardAvoidingView>
  );
}

CreatePost.propTypes = {
  user: PropTypes.object,
  onPostDidChange: PropTypes.func,
  onSetMedia: PropTypes.func,
  onLocationDidChange: PropTypes.func,
  blurInput: PropTypes.func,
  inputRef: PropTypes.any,
};

export default withNavigation(CreatePost);
