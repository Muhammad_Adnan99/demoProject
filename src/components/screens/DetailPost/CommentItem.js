import React from 'react';
import { Text, View, Image } from 'react-native';
import { useColorScheme } from 'react-native-appearance';
import PropTypes from 'prop-types';

import AudioMediaThreadItem from '../../../../src/Core/chat/IMChat/AudioMediaThreadItem';
import dynamicStyles from './styles';

function CommentItem(props) {
  const {
    appStyles,
    item: { firstName, commentText, profilePictureURL, audioURL },
  } = props;
  const colorScheme = useColorScheme();
  const styles = dynamicStyles(colorScheme);

  return (
    <View style={styles.commentItemContainer}>
      <View style={styles.commentItemBodyContainer}>
        <View style={styles.commentItemBodyRadiusContainer}>
          <Text style={styles.commentItemBodyTitle}>{firstName}</Text>

          {audioURL ? (
            <AudioMediaThreadItem
              outBound={false}
              item={audioURL}
              appStyles={appStyles}
            />
          ) : (
            <Text style={styles.commentItemBodySubtitle}>{commentText}</Text>
          )}
        </View>
      </View>

      <View style={styles.commentItemImageContainer}>
        <Image
          style={styles.commentItemImage}
          source={{
            uri: profilePictureURL,
          }}
        />
      </View>
    </View>
  );
}

CommentItem.propTypes = {
  item: PropTypes.object,
};

export default CommentItem;
