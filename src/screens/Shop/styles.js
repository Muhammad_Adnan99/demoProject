import { StyleSheet } from 'react-native';
import mainStyles from '../../mainStyles';
import { responsiveWidth } from 'react-native-responsive-dimensions';

const styles = new StyleSheet.create({
  ...mainStyles,
});

export default styles;
